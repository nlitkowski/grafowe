from sys import argv
import argparse
import copy
from math import inf
from itertools import product


def main(filename: str):
    graph = {}
    i = 0
    with open(filename, "r") as fd:
        for line in fd:
            i += 1
            graph[i] = parse_line(line)
    find_cycles(graph)


def parse_line(line: str) -> list:
    items = line.split()
    neighbours_list = []
    for idx, item in enumerate(items, start=1):
        if "-" != item:
            neighbours_list.append(idx)
    return neighbours_list


def find_cycles(graph: dict):
    stack = []
    already_added = {}
    current = 1
    root = 1
    while True:
        stack.append(current)
        print(" ".join(str(x) for x in stack))
        next = get_next(root, stack, graph, already_added)
        if next == -1:
            already_added[stack.pop()] = []
            current = stack.pop()
            if stack == []:
                if current == root:
                    print(current)
                break
        elif next == root:  # cycle found
            print(f"CYKL HAMILTONA: {' '.join(str(x) for x in (stack + [root]))}")
            already_added[stack.pop()] = []
            current = stack.pop()
        elif current not in already_added.keys():
            already_added[current] = [next]
            current = next
        else:
            already_added[current].append(next)
            current = next


def get_next(root: int, stack: list, graph: dict, already_added: dict) -> int:
    for i in sorted(graph[stack[len(stack) - 1]]):
        if i == root and len(stack) == len(graph):
            return root
        elif i not in stack:
            if stack[len(stack) - 1] not in already_added.keys():
                return i
            elif i not in already_added[stack[len(stack) - 1]]:  # current vertex
                return i
    return -1  # can't build further


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument(
        "--filename",
        nargs="?",
        type=str,
        default="graph09.txt",
        help="File to load graph definition from",
    )
    args = parser.parse_args()
    main(args.filename)
