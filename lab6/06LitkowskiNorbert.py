from sys import argv
import argparse
import copy
from math import inf
from itertools import product


def main(filename: str):
    graph = {}
    i = 0
    with open(filename, "r") as fd:
        for line in fd:
            i += 1
            graph[i] = parse_line(line)


def parse_line(line: str) -> list:
    items = line.split()
    neighbours_list = []
    for idx, item in enumerate(items, start=1):
        if "-" != item:
            neighbours_list.append(idx)
    return neighbours_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument(
        "--filename",
        nargs="?",
        type=str,
        default="graph11.txt",
        help="File to load graph definition from",
    )
    args = parser.parse_args()
    main(args.filename)
