from sys import argv
import argparse
import copy
from math import inf
from itertools import product


def main(filename: str):
    neighbours = {}
    with open(filename, "r") as fd:
        i = 1
        for line in fd:
            neighbours[i] = parse_weight_line(line)
            i += 1
    floyd_warshall(len(neighbours), neighbours)


def parse_weight_line(line: str) -> dict:
    weights = line.split()
    ret_dict = {}
    for idx, item in enumerate(weights, 1):
        if not "-" == item:
            w = int(item)
            ret_dict[idx] = w
    return ret_dict


def get_predecessor_string(element) -> str:
    if element is None:
        return "None"
    return str(element + 1)


def floyd_warshall(vertices_count: int, neighbours: dict):
    vertices_list = range(vertices_count)
    distances = [[inf] * vertices_count for i in vertices_list]
    predeccessors = [[None] * vertices_count for i in vertices_list]
    negative_cycle = False
    for u in neighbours:
        for v in neighbours[u]:
            w = neighbours[u][v]
            distances[u - 1][v - 1] = w
            predeccessors[u - 1][v - 1] = u - 1
    for k in vertices_list:
        d_prev, p_prev = distances, predeccessors
        print(f"W{k}=")
        for t in distances:
            print(" ".join(str(dist) for dist in t))
        print(f"P{k}=")
        for t in predeccessors:
            print(" ".join(map(lambda x: get_predecessor_string(x), t)))
        for i, j in product(vertices_list, repeat=2):
            if i != k and j != k:
                if d_prev[i][j] > d_prev[i][k] + d_prev[k][j]:
                    distances[i][j] = d_prev[i][k] + d_prev[k][j]
                    predeccessors[i][j] = p_prev[k][j]
                    if distances[i][j] < 0:
                        negative_cycle = True
        if negative_cycle:
            print(f"W{k + 1}=")
            for t in distances:
                print(" ".join(str(dist) for dist in t))
            print(f"P{k + 1}=")
            for t in predeccessors:
                print(" ".join(map(lambda x: get_predecessor_string(x), t)))
            print("Ujemny cykl. Nie ma rozwiązania.")
            return
    print("Ostateczna macierz odleglosci:")
    for t in distances:
        print(" ".join(str(d) for d in t))
    print("Ostateczna macierz poprzednikow:")
    for t in predeccessors:
        print(" ".join(map(lambda x: get_predecessor_string(x), t)))
    # for i in vertices_list:
    # Not iterating over all vertices, only first
    print("Najkrotsze sciezki:")
    for i in vertices_list[:1]:
        for j in vertices_list:
            path = [i]
            t = 0
            vert = 0
            while t < vertices_count:
                if predeccessors[j][vert] == j:
                    t = vertices_count
                    path.append(predeccessors[j][vert])
                else:
                    path.append(predeccessors[j][vert])
                    vert = predeccessors[j][vert]
                    t = t + 1
            print(f"z {i + 1} do {j + 1} : {' '.join(str(v + 1) for v in path)}")


def pretty_print_neighbours(neighbours: dict):
    neighbours_list = {}
    for key in neighbours:
        t_list = []
        for vert in neighbours[key]:
            t_list.append(vert)
        neighbours_list[key] = sorted(t_list)
    print("Lista nastepnikow:")
    for key in neighbours_list:
        print(f"{key} : {' '.join(map(lambda x: str(x), neighbours_list[key]))}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument(
        "--filename",
        nargs="?",
        type=str,
        default="graph05bez.txt",
        help="File to load graph definition from",
    )
    args = parser.parse_args()
    main(args.filename)
