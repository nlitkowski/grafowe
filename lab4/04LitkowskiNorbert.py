from sys import argv
import argparse
import copy
from math import inf
from itertools import product


def main(filename: str):
    graph = []
    with open(filename, "r") as fd:
        for line in fd:
            graph.append(parse_weight_line(line))
    prim(graph, len(graph))


def parse_weight_line(line: str) -> list:
    weights = line.split()
    ret_list = []
    for _, item in enumerate(weights):
        if "-" == item:
            ret_list.append(inf)
        else:
            w = int(item)
            ret_list.append(w)
    return ret_list


def find_next(labels, included, numVertices):
    min_w = inf
    for i in range(numVertices):
        _, w = labels[i]
        if w < min_w and included[i] == False:
            min_w = w
            min_index = i
    return min_index


def print_labels(labels):
    line_to_print = ""
    for _, item in enumerate(labels):
        prev, weight = item
        if weight == inf:
            weight = "inf"
        if prev == -1:
            prev = None
        if prev != None:
            prev = prev + 1
        line_to_print = line_to_print + f"[{prev},{weight}] "
    print(line_to_print)


def prim(graph, numVertices):
    labels = [(None, inf)] * numVertices
    included = [False] * numVertices
    labels[0] = (-1, 0)
    for i in range(1, numVertices + 1):
        vert_idx = find_next(labels, included, numVertices)
        included[vert_idx] = True
        print(f"Rozpatrywany wierzcholek: {vert_idx + 1}")
        weights = graph[vert_idx]
        for idx, item in enumerate(weights):
            __, weight = labels[idx]
            if weight > item and included[idx] == False:
                labels[idx] = (vert_idx, item)
        if i < numVertices:  # do not print last update
            print_labels(labels)
    edges_line = ""
    sum_weights = 0
    for i in range(1, len(labels)):
        prev, weight = labels[i]
        sum_weights = sum_weights + weight
        edges_line = edges_line + f"({i + 1},{prev + 1}),"
    print(f"Krawedzie drzewa: {edges_line}")
    print(f"Waga drzewa: {sum_weights}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument(
        "--filename",
        nargs="?",
        type=str,
        default="graph07.txt",
        help="File to load graph definition from",
    )
    args = parser.parse_args()
    main(args.filename)
