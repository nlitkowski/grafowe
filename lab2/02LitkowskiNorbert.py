from sys import argv
import argparse
import copy


def main(filename: str):
    neighbours = {}
    with open(filename, "r") as fd:
        i = 1
        for line in fd:
            neighbours[i] = parse_weight_line(line)
            i += 1
    already_checked = []
    for source in neighbours:
        for target in neighbours[source]:
            if (source, target) not in already_checked and (
                target,
                source,
            ) not in already_checked:
                already_checked.append((source, target))
                if BFS(neighbours, source, target):
                    print(f"({source}, {target}) NIE")
                else:
                    print(f"({source}, {target}) TAK")


def parse_weight_line(line: str) -> list:
    weights = line.split()
    ret_list = []
    for idx, item in enumerate(weights, 1):
        if not "-" == item:
            ret_list.append(idx)
    return ret_list


def BFS(original_neighbours, source, target) -> bool:
    neighbours = copy.deepcopy(original_neighbours)
    neighbours[source].remove(target)
    visited = [False] * (len(neighbours))
    queue = []
    queue.append(source)
    visited[source - 1] = True
    while queue:
        source = queue.pop(0)
        for i in neighbours[source]:
            if i == target:
                return True
            if visited[i - 1] == False:
                queue.append(i)
                visited[i - 1] = True
    return False


def pretty_print_neighbours(neighbours: dict):
    neighbours_list = {}
    for key in neighbours:
        t_list = []
        for vert in neighbours[key]:
            t_list.append(vert)
        neighbours_list[key] = sorted(t_list)
    print("Lista nastepnikow:")
    for key in neighbours_list:
        print(f"{key} : {' '.join(map(lambda x: str(x), neighbours_list[key]))}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument(
        "--filename",
        nargs="?",
        type=str,
        default="graph03.txt",
        help="File to load graph definition from",
    )
    args = parser.parse_args()
    main(args.filename)
