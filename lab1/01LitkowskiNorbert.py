from sys import argv
import argparse

def main(filename: str):
    with open(filename, "r") as fd:
        vertices_number = int(fd.readline().strip())
        neighbours = {}
        for i in range(1, vertices_number + 1):
            neighbours[i] = parse_weight_line(fd.readline())
        pretty_print_neighbours(neighbours)
        for line in fd:
            parse_edge_line(line, neighbours)
        pretty_print_neighbours(neighbours)

def parse_weight_line(line: str) -> dict:
    weights = line.split()
    ret_dict = {}
    for idx, item in enumerate(weights, 1):
        if not "-" == item:
            w = int(item)
            ret_dict[idx] = w
    return ret_dict

def parse_edge_line(line: str, neighbours: dict):
    v1, v2 = map(lambda x: int(x), line.split())
    if v2 in neighbours[v1]:
        del neighbours[v1][v2]
        del neighbours[v2][v1]
    else:
        neighbours[v1][v2] = 3
        neighbours[v2][v1] = 3


def pretty_print_neighbours(neighbours: dict):
    neighbours_list = {}
    edges_with_weights = {}
    for key in neighbours:
        t_list = []
        for vert in neighbours[key]:
            t_list.append(vert)
            if (key,vert) and (vert, key) not in edges_with_weights:
                edges_with_weights[(key, vert)] = neighbours[key][vert]
        neighbours_list[key] = sorted(t_list)
    print(neighbours_list)
    print(neighbours)
    print(edges_with_weights)
    print("Lista nastepnikow:")
    for key in neighbours_list:
        print(f"{key} : {' '.join(map(lambda x: str(x), neighbours_list[key]))}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Operations on graph")
    parser.add_argument("--filename", nargs="?", type=str, default="graph.txt",
                        help="File to load graph definition from")
    args = parser.parse_args()
    main(args.filename)
